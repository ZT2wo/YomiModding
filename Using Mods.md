## Using Mods

**WARNING**: Mods contain arbitrary code, which will be executed with the same privileges as the game itself. An evil mod may contain malware which can take over your computer, and destroy or steal your data. Do not run mods from untrusted sources.

Mods are distributed as `.zip` files. One `.zip` file contains one mod.

To install a mod, go to the game's installation directory (the directory containing `YomiHustle.exe`) and place the mod `.zip` files in the `mods` directory. If there isnt a `mods` directory, create one. Do not unpack the `.zip` files.

Additionally, if you are playing the game using the Steam version, you can use the Steam Workshop to have the process of downloading mods be done to you automatically. Mods downloaded by the workshop will not be stored in the `mods` directory, and will instead be located in a separate folder, handled by Steam. This means that you need to open the game through Steam in order to play with the workshop mods.

### Online Play

In order to play online with mods, you will need to have every server-sided mod your opponent has installed. The modloader will verify that you both have identical loaded server-sided mods, and if there is a difference, prevent you from playing. 

If you have a client-side mod, this verification will not occur, meaning that you can use it in online matches with no issues.
