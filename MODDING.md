# Community Modding System for Your Only Move Is HUSTLE

This document describes the modding system (loader and API) included with the game Your Only Move Is HUSTLE.

## Writing Mods

Example Mod: https://gitlab.com/ZT2wo/YomiModding/-/tree/main/Example%20Mod

### Mod structure

There should not be any files in the root directory of the `.zip` file - instead, all of the mod's files should be in a directory, named after the mod. For example, a mod which adds a big ninja shuriken, distributed as `NinjaBigShuriken.zip`, should have all its files in a directory called `NinjaBigShuriken`. This ensures that the mod does not accidentally interfere with parts of the game or other mods.

At the moment, the only "special" files inside the ZIP file are files called `ModMain.gd` and `_metadata`. The mod loader will load and instantiate all such files as scripts; it is then the duty of these scripts to use the mod loader API (see below) to install hooks or load additional resources. `ModMain.gd` scripts should extend `Node`.

Files which override or extend game scripts should, by convention, have the same path as the game script; so, if the mod NinjaBigShuriken wants to expand the game script `characters/stickman/projectiles/fireball_states/ShurikenDefault.gd`, it should have the extending script at `NinjaBigShuriken/characters/stickman/projectiles/fireball_states/ShurikenDefault.gd`. Following this convention will make it easier to collaborate on mods, and may help with relative path issues.

If a mod does not include both a `ModMain.gd` and a `_metadata`, the mod loader will not load the mod, and will output to console what has prevented the file from not being loaded.

### Metadata

The `_metadata` is a required file used to communicate information about the installed mod. It needs to be structured as below. While the `_metadata` file is a structured JSON file, it should not include ANY file extension.

```
{
  "name": "wizardOP",
  "friendly_name": "Wizard OP",
  "description": "Makes Magic Dart cast 2 darts",
  "author": "ZT2wo",
  "version": "1.0",
  "link": "",
  "id": "00001",
  "requires": [""],
  "overwrites": false,
  "client_side": false,
  "priority": 0,
}
```

Metadate Fields:
- Name: Internal name of mod. Used to communicate between mods for dependencies.
- Friendly_name: Publicly used name of mod. This is displayed to the player, so make it readable.
- Description: Description of what the mod does/adds/changes.
- Author: Author of the mod, recommended to put Discord or Twitter tag.
- Version: Version of mod, used for version control/compatibility.
- Link: Link to where the mod is hosted. Will be used in the future.
- ID: Unique ID for mod. Will be used in future for Steam Workshop support. 
- Requires: List of dependencies for the mod. Comma-seperated array of the dependency mods' names.
- Overwrites: Boolean for determining if the mod is an asset replacement mod. Works for Character Sprites and Sounds replacements.
- Client Side: Boolean for determining if the mod works without affecting multiplayer. 
- Priority: Determines the load order. See **Priority** section below

### Priority

Priority helps sort the mods into an order of importance during loading, controlling the order in which ModMain.gd scripts are executed - from lowest to highest priority. The default priority is 0.

If a mod is a required dependency for other mods, it should had a negative priority.

### Initialization

`ModMain.gd` scripts are initialized in two important steps, each being customizable using a Godot hook function:

#### `_init`

The `_init` function looks like this:

```gdscript
func _init(modLoader = ModLoader):
	print("Initializing my mod")
```

At the time when `_init` is called, singletons (except `Tool` and `Debug`) are not available; this includes `ModLoader`, so the mod loader passes itself as a parameter to `_init`.

#### `_ready`

The `_ready` hook looks like usual:

```gdscript
func _ready():
    print("My mod is ready")
```

#### `_init` or `_ready`?

`_init` is appropriate for installing script extensions. Specifically, singletons (autoloads) can only be extended from `_init`, before they are intantiated.

At the time when `_ready` is called, all singletons are already available. Any initialization code which needs to access e.g. `CurrentGame` is better suited for `_ready`.

Godot scene tree manipulation is better suited for `_ready` as well.

### Modding API
The functions can be invoked as `ModLoader.functionNameHere(...)`, except in `_init`, where they must be invoked via the passed parameter (`modLoader.functionNameHere(...)`).

#### `installScriptExtension`

Given a path to a script which extends some game script, replaces the original game script with the given extension script.

```gdscript
func installScriptExtension(
	# res:// path to the extending script
	childScriptPath:String
)
```

See the section "Script inheritance" below for more details.

#### `appendNodeInScene`

Helper function: create and add a node to a instanced scene.

```gdscript
func appendNodeInScene(
	# an instanced scene of a PackedScene you want to replace
	modifiedScene:Scene, 

	# name of the new node
	nodeName:String = "", 

	# path to the parent node in the PackedScene
	nodeParent = null, 

	# path used to instanciate the node
	instancePath:String = "", 

	# node visibility state
	isVisible:bool = true
)
```

#### `saveScene`

Helper function: save the scene as a PackedScene, overwriting Godot's cache if needed.

```gdscript
func saveScene(
	# an instanced scene of a PackedScene you want to replace
	modifiedScene:Scene, 

	# path to the PackedScene
	scenePath:String
)
```

#### `textureGet`

Helper function: load an image as a Texture, made by perez.

```gdscript
func textureGet(
	# path to the image
	imagePath:String
)
```

# `Overwriting character assets`

As of 1.0 character "asset replacement packs" are supported.

Example Asset Replacement Pack : https://gitlab.com/ZT2wo/YomiModding/-/tree/main/Example%20Asset%20Replacement

### File Structure:
Overwriting sprites only requires matching character names, and animation names.
Overwriting sounds requires matching the state name for states, and the sound name for base sounds.

So if you want to overwrite Ninjas Chuk Air Spin, the folder structure would be as such:

--|(The Zip Containing your mod)

----|(The folder named after your mod)

------|Overwrites (This folder must be named exactly the same)

--------|Ninja (A folder named after the Character)

----------|NunChukSpin (This folder, and any others, are named after the animation they replace)

------------>0.png (The png of your sprites labeled by frame number)

------------>1.png

------------>2.png

------------>...


If you want to overwrite Ninjas Parry sound and Summon sound, the folder structure would be as such:

--|(The Zip Containing your mod)

----|(The folder named after your mod)

------|Overwrites (This folder must be named exactly the same)

--------|Ninja (A folder named after the Character)

----------|Sounds (Folder)

------------|BaseSounds (Folder)

--------------|Parry.wav (These sounds are for the sounds visable in the node tree)

--------------|...

------------|StateSounds (Folder)

--------------|Summon.wav (Named after the state they modify the sound of)

--------------|...

### A resource for animation names and their frame indeicies can be found here:
### https://github.com/lucastucious/YomiHustle-spritedata/blob/master/BaseChar/BaseChar-SpriteData.md
#### Resource Provided by: LUCASTUCIOUS

## Technical Architecture

### Overview

The mod loader does two things:

1. Plug the mod `.zip` file into the Godot resource virtual filesystem using `ProjectSettings.load_resource_pack`

2. Search for file entries called `ModMain.gd` in the `.zip` file. For every such entry found, a script is loaded and instantiated from the equivalent Godot resource virtual filesystem path (`res://...`). The script instances are added as children nodes of the `ModLoader` script.

### Script inheritance

In order to allow modifying game functions without copying the contents of the functions or entire scripts in mod code, this system makes use of inheritance for hooking. We exploit the fact that all Godot scripts are also classes and may extend other scripts in the same way how a class may extend another class; this allows mods to include script extensions, which extend some standard game script and selectively override some methods.

To install this script extension, call `modLoader.installScriptExtension` from your mod's `ModMain.gd`, in `_init`:

```gdscript
extends Node

func _init(modLoader = ModLoader):
	modLoader.installScriptExtension('res://MyMod/CurrentGame.gd')
```

### Inheritance chaining

To allow multiple mods to customize the same base script, `installScriptExtension` takes care to enable inheritance chaining. In practice, this is transparent to mods, and things should "just work".

If a script given to `installScriptExtension` extends from a game script which is already extended, then it will be extended from the last child script. For example, if we have two mods with a `extends "res://CurrentGame.gd"` script, then the first mod's script will extend from the game's `CurrentGame`, but after this `"res://CurrentGame.gd"` will point at the extended script, so the second mod's script will extend from the first mod's script instead.

### Modlist Hashing 

To allow online play to avoid desyncs, the modloader will hash all .zip files placed in the `mods` folder, and compare the list of hashes between players on loddy connection. If there is a mismatch, the lobby will error and prevent play from happening. Players will need to compare modlists in order to play. 

In the future, the modloader will only compare the hashes of mods that will affect the game, such as selected modded characters, and any mods that affect the overall game. 

### Access Prevention

In order to prevent desyncs, the modloader prevents any scripts from extending `Network.gd`. 

## Credits and more info
Documentation modfied from: https://gitlab.com/Delta-V-Modding/Mods/-/blob/main/MODDING.md

Your Only Move Is HUSTLE modding structure: ZT2wo#9157 and Ted#0420


