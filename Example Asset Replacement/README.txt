In _metadata edit the following:

"name": "a camelCase name"

"friendly_name": "the name that the player will see"

"description": "a short description of your texture"

"author": "your name"

"version": "wich version your texture currently is"

-to maintain formality, use the number before the dot for official releases,
and the number after the dot for patches

"id": "a random 5 number combination, currently not used, but put one just for the sake of it"

"overwrites" : "true"

"client_side" : "true"



"link" is optional

"requires" is dependent of if your asset replacement pack needs other mods

"priority" is dependent on if you want your mod to load before/after other mods, keep it at 0 for safety.
