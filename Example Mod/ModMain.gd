extends Node

const MOD_PRIORITY = 1

func _init(modLoader = ModLoader):
    print("Wizard Mod Loaded")
    modLoader.installScriptExtension("res://wizardOP/FlameWave.gd")
    modLoader.installScriptExtension("res://wizardOP/MagicMissle.gd")